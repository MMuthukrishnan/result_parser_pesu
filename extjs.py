import json
import yaml
import requests
import csv
import pickle

#subjdict={"UE15EC301":2,"UE15EC302":3,"UE15EC303":4,"UE15EC305":5,"UE15EC306":6,"UE15EC311":7,"UE15EC312":8,"UE15EC313":9,"UE15EC314":10,"UE15EC315":11,"UE15EC316":12,"UE15EC322":13,"UE15EC323":14,"UE15EC324":15,"UE15EC326":16,"SGPA":17}

#subjdict={"SGPA":2,"UE15EC351":3,"UE15EC352":4,"UE15EC353":5,"UE15EC331":6,"UE15EC332":7,"UE15EC333":8,"UE15EC334":9,"UE15EC335":10,"UE15EC336":11,"UE15EC337":12,"UE15EC341":13,"UE15EC342":14,"UE15EC343":15,"UE15EC346":16,"UE15EC347":17,"UE15IE301":18,"UE15EC354":19,"UE15EC355":20,"UE15EC356":21}

#subjdict={"SGPA":1,"UE15EC401":2,"UE15EC402":3,"UE15EC403":4,"UE15EC404":5,"UE15EC405":6,"UE15EC406":7,"UE15EC411":8,"UE15EC412":9,"UE15EC413":10,"UE15EC414":11,"UE15EC415":12,"UE15EC416":13,"UE15EC421":14,"UE15EC422":15,"UE15EC423":16,"UE15EC424":17,"UE15EC425":18,"UE15EC425":19,"UE15EC426":20,"UE15EC427":21,"UE15EC428":22}

subjdict={"SGPA":3,"UE15EC401":4,"UE15EC402":5,"UE15EC403":6,"UE15EC411":7,"UE15EC412":8,"UE15EC413":9,"UE15EC414":10,"UE15EC415":11,"UE15EC416":12,"UE15EC421":13,"UE15EC422":14,"UE15EC423":15,"UE15EC424":16,"UE15EC425":17,"UE15EC426":18,"UE15EC427":19,"UE15EC428":20}


usnfile=open("dldusn","r")
resfile=open("res.csv","a")
wr = csv.writer(resfile, dialect='excel')

wr.writerow(["USN","Name","SGPA","UE15EC401","UE15EC402","UE15EC403","UE15EC411","UE15EC412","UE15EC413","UE15EC414","UE15EC415","UE15EC416","UE15EC421","UE15EC422","UE15EC423","UE15EC424","UE15EC425","UE15EC426","UE15EC427","UE15EC428"])

mlist=[]
for line in usnfile:
   resfile=open("res.csv","a")
   wr = csv.writer(resfile, dialect='excel')

   try : 
      sq=line.split(",")

      #r = requests.get('https://pesuacademy.com/Academy/tr/result/'+sq[0],verify=False)


      # To use the batch downloaded json files

      f = open('/home/krishna/test/oth/result_parser_pesu/batch_downloader/dld/'+sq[0].strip())
      data = json.load(f)

      #data = f.json()
      #data=json.dumps(data)
      #data=yaml.load(data)

      l=['-']*22
      l[0]=sq[0]
      #l[1]=sq[1].rstrip("\r\n")

      for record in data["results"]:
         subjectCode=record.get("subjectCode")
         indx=subjdict.get(subjectCode,-1)
         if(indx!=-1):
            l[indx]=record.get("grade")


      wr.writerow(l)
      mlist.append(l)
      print(sq[0])  


         #usnfile.close()
      resfile.close()  


      with open('pickleoutfile', 'wb') as fpick:
         pickle.dump(mlist, fpick)
   
   except:
      print("IT BE EMPTY")
